//
// Created by hg on 1/28/24.
//

#include "MainFrame.h"

#include <string>
#include <wx/wx.h>

#include "AppContext.h"


MainFrame::MainFrame(const wxString& title, const wxSize& startingSize)
    : wxFrame(nullptr, wxID_ANY, title, wxDefaultPosition, startingSize) {
    // Main container is a 9x9 grid. We'll place the relevant info in the center block
    auto* mainPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, startingSize);
    auto* mainBox = new wxBoxSizer(wxVERTICAL);

    const auto margin{1};
    auto* flexSizer = new wxFlexGridSizer(3, 3, margin, margin);

    // Columns and rows around the center block will be empty but growable
    flexSizer->AddGrowableCol(0, 1);
    flexSizer->AddGrowableCol(2, 1);
    flexSizer->AddGrowableRow(0, 1);
    flexSizer->AddGrowableRow(2, 1);

    for (int i{0}; i < 3; ++i) {
        for (int j{0}; j < 3; ++j) {
            auto* block = new wxPanel(mainPanel, wxID_ANY, wxDefaultPosition, wxSize{0, 0});

            // Place the controls in the center slot
            if ((i == 1) && (j == 1)) {
                phaseDisplay = new wxStaticText(block, wxID_ANY, app_context_.getCurrentPhaseString(),
                                                wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);
                phaseDisplay->SetFont(app_context_.settings.fonts[AppSettings::idPhaseDisplay]);

                auto completedPomodorosText = new wxStaticText(block, wxID_ANY,
                                                               "Pomodoros",
                                                               wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);
                completedPomodorosText->SetFont(app_context_.settings.fonts[AppSettings::idWorkCountDisplay]);

                workCountDisplay = new wxStaticText(block, wxID_ANY,
                                                    std::to_string(
                                                        app_context_.getWorkCount()),
                                                    wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);
                workCountDisplay->SetFont(app_context_.settings.fonts[AppSettings::idWorkCountDisplay]);

                timerDisplay = new wxStaticText(block, wxID_ANY,
                                                app_context_.pomodoro_.getFormattedRemainingTime().c_str(),
                                                wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);
                timerDisplay->SetFont(app_context_.settings.fonts[AppSettings::idTimerDisplay]);

                startStopButton = new wxButton(block, wxID_ANY, wxT("Start"));

                auto* infoSizer = new wxBoxSizer(wxVERTICAL);
                // NOTE: don't use the flag wxSHAPED, it will cut off parts of strings of the phaseDisplay control
                infoSizer->AddStretchSpacer();
                infoSizer->Add(phaseDisplay, 0, wxALIGN_CENTER);
                infoSizer->AddStretchSpacer();
                infoSizer->Add(completedPomodorosText, 0, wxALIGN_CENTER);
                infoSizer->Add(workCountDisplay, 0, wxALIGN_CENTER);
                infoSizer->AddStretchSpacer();
                infoSizer->Add(timerDisplay, 0, wxALIGN_CENTER);
                infoSizer->AddStretchSpacer();
                infoSizer->AddStretchSpacer();
                infoSizer->Add(startStopButton, 0, wxALIGN_CENTER);
                infoSizer->AddStretchSpacer();

                block->SetSizer(infoSizer);

                // Set a minimun size for the center block
                block->SetMinSize(startingSize);
            }
            flexSizer->Add(block, 1, wxEXPAND);
        }
    }

    // Event handlers are executed in the main thread. It gives the illusion of parallelism without the complexities of
    // multithreading
    timer.SetOwner(this);

    this->Bind(wxEVT_TIMER, [this](wxTimerEvent& ev) {
        app_context_.handleTimerEvent(timer);

        // Update UI elements
        // NOTE: SetLabel internally checks if the string changed. If so, it updates accordingly
        phaseDisplay->SetLabel(app_context_.getCurrentPhaseString());

        workCountDisplay->SetLabel(std::to_string(app_context_.getWorkCount()));

        if (app_context_.isPhaseRunning()) {
            startStopButton->SetLabel("Stop");
        }
        else {
            startStopButton->SetLabel("Start");
        }

        timerDisplay->SetLabel(app_context_.pomodoro_.getFormattedRemainingTime());

        // Force a recomputation of the layout since we modified the contents of some of the controls
        this->GetSizer()->Layout();
    });


    startStopButton->Bind(wxEVT_BUTTON, [this](wxCommandEvent& ev) {
        app_context_.handleButton(timer);

        // Update UI elements only when Timer/Pomodoro is stopped
        if (!app_context_.isPhaseRunning()) {
            timerDisplay->SetLabel(app_context_.pomodoro_.getFormattedRemainingTime());
            startStopButton->SetLabel("Start");
        }

        // Force a recomputation of the layout since we modified the contents of some of the controls
        this->GetSizer()->Layout();
    });

    mainPanel->SetSizerAndFit(flexSizer);
    mainBox->Add(mainPanel, 1, wxEXPAND);
    this->SetSizerAndFit(mainBox);
    // Display the main window (MainFrame) at the center of the screen
    this->Centre();
}
