//
// Created by hg on 1/28/24.
//

#include "WidgetMainApp.h"
#include "MainFrame.h"

#include <wx/wx.h>



IMPLEMENT_APP(WidgetMainApp)

bool WidgetMainApp::OnInit() {
    auto* frame = new MainFrame(wxT("Tomate"), wxSize{250, 300});
    frame->Show(true);

    return true;
}
