//
// Created by hg on 1/28/24.
//

#ifndef WIDGETMAINAPP_H
#define WIDGETMAINAPP_H


#include <wx/wx.h>



class WidgetMainApp : public wxApp {
public:
    bool OnInit() override;
};



#endif //WIDGETMAINAPP_H
