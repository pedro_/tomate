//
// Created by hg on 1/28/24.
//

#ifndef MAINFRAME_H
#define MAINFRAME_H

#include "AppContext.h"

#include <wx/wx.h>



class MainFrame : public wxFrame {
public:
    explicit MainFrame(const wxString& title, const wxSize& startingSize);


    wxTimer timer;
    wxButton* startStopButton;
    wxStaticText* phaseDisplay;
    wxStaticText* timerDisplay;
    wxStaticText* workCountDisplay;

private:
    AppContext app_context_;
};


#endif //MAINFRAME_H
