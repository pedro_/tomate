//
// Created by hg on 2/3/24.
//

#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include "Pomodoro.h"

#include <unordered_map>
#include <wx/wx.h>


#if DEBUG
constexpr unsigned char defaultFocusMinutes = 0;
constexpr unsigned char defaultFocusSeconds = 6;
constexpr unsigned char defaultShortRestMinutes = 0;
constexpr unsigned char defaultShortRestSeconds = 2;
constexpr unsigned char defaultLongRestMinutes = 0;
constexpr unsigned char defaultLongRestSeconds = 4;
#else
// Use a value between 0 and 59
constexpr unsigned char defaultFocusMinutes = 25;
constexpr unsigned char defaultFocusSeconds = 0;
constexpr unsigned char defaultShortRestMinutes = 5;
constexpr unsigned char defaultShortRestSeconds = 0;
constexpr unsigned char defaultLongRestMinutes = 15;
constexpr unsigned char defaultLongRestSeconds = 0;
//
#endif // DEBUG


enum Phase {
    Focus,
    ShortRest,
    LongRest,
};

// Data class
struct AppSettings {
    static constexpr int idPhaseDisplay = 1;
    static constexpr int idTimerDisplay = 2;
    static constexpr int idWorkCountDisplay = 3;

    // map
    std::unordered_map<int, wxFont> fonts{
        {idPhaseDisplay, wxFontInfo{14}.FaceName("Liberation Sans")},
        {idTimerDisplay, wxFontInfo{62}.FaceName("Liberation Sans")},
        {idWorkCountDisplay, wxFontInfo{12}.FaceName("Liberation Sans")},
    };
};


class AppContext {
public:
    AppContext() = default;

    AppContext(AppContext& other) = default;

    bool isLongRest() const;

    const std::string& getCurrentPhaseString();

    const size_t& getWorkCount() const;

    bool isPhaseRunning();

    bool isPhaseFinished();

    // TODO: if this function will be only used internally, make it private and remove the argument `phase`
    void updatePhase(Phase& phase);

    void handleButton(wxTimer& timer);

    void handleTimerEvent(wxTimer& timer);

    Phase phase{Focus};
    Pomodoro pomodoro_{Pomodoro::Minutes{defaultFocusMinutes}, Pomodoro::Seconds{defaultFocusSeconds}};
    size_t workPhaseCounter_{0};
    AppSettings settings;

private:
    std::unordered_map<Phase, std::string> phaseToString_{
        {Focus, "Focus"}, {ShortRest, "Short Rest"}, {LongRest, "Long Rest"}
    };

    void setPomodoroTime();
};


#endif //APPCONTEXT_H
