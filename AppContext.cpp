//
// Created by hg on 2/3/24.
//

#include "AppContext.h"


bool AppContext::isLongRest() const {
    return (workPhaseCounter_ % 4) == 0;
}


const std::string& AppContext::getCurrentPhaseString() {
    return phaseToString_[phase];
}


const size_t& AppContext::getWorkCount() const {
    return workPhaseCounter_;
}


bool AppContext::isPhaseRunning() {
    return pomodoro_.isRunning();
}


bool AppContext::isPhaseFinished() {
    return pomodoro_.isFinished();
}


void AppContext::updatePhase(Phase& phase) {
    // First set the next phase based on the current one
    if (phase == Focus) {
        ++workPhaseCounter_;
        phase = isLongRest() ? LongRest : ShortRest;
    }
    else {
        phase = Focus;
    }

    setPomodoroTime();
}


void AppContext::handleButton(wxTimer& timer) {
    if (pomodoro_.isRunning()) {
        timer.Stop();
        pomodoro_.stop();
        // Restore the phase's initial time
        setPomodoroTime();
    }
    else {
        timer.Start(1000);
        // BUGFIX: Don't start the pomodoro here. If you do, it will make the countdown to skip a second at the
        // beginning because Pomodoro::getFormattedRemainingTime() rounds towards 0. The `start()` method must be
        // called when the timer callback occurs for the first time in a session of rounding down to 0.
        // pomodoro_.start();
    }
}


void AppContext::handleTimerEvent(wxTimer& timer) {
    // BUGFIX. See the comment in `handleButton()`
    if (!pomodoro_.isRunning()) {
        pomodoro_.start();
    }

    pomodoro_.updateRemainingTime();
    if (pomodoro_.isFinished()) {
        timer.Stop();
        updatePhase(phase);
    }
}

//
//  Private methods
//
void AppContext::setPomodoroTime() {
    // Set the time based on the phase that is about to start
    switch (phase) {
        case Focus:
            pomodoro_ = Pomodoro{Pomodoro::Minutes{defaultFocusMinutes}, Pomodoro::Seconds{defaultFocusSeconds}};
            break;
        case ShortRest:
            pomodoro_ = Pomodoro{
                Pomodoro::Minutes{defaultShortRestMinutes}, Pomodoro::Seconds{defaultShortRestSeconds}
            };
            break;
        case LongRest:
            pomodoro_ = Pomodoro{
                Pomodoro::Minutes{defaultLongRestMinutes}, Pomodoro::Seconds{defaultLongRestSeconds}
            };
            break;
    }
}
