//
// Created by hg on 1/11/24.
//

#include "Pomodoro.h"

#include <fmt/core.h>
#include <fmt/chrono.h>


Pomodoro::Pomodoro(const Minutes m, const Seconds s) : minutes_{m}, seconds_{s} {
    remainingTime_ = minutes_ + seconds_;
}

void Pomodoro::start() {
    state_ = Running;
    targetTime_ = SteadyClock::now() + minutes_ + seconds_;
}

void Pomodoro::stop() {
    state_ = Start;
}

void Pomodoro::updateRemainingTime() {
    remainingTime_ = targetTime_ - SteadyClock::now();
    if (isFinished()) {
        state_ = Start;
    }
}

Pomodoro::SteadyClock::duration Pomodoro::getRemainingTime() {
    return remainingTime_;
}

bool Pomodoro::isFinished() {
    return remainingTime_.count() <= 0;
}

bool Pomodoro::isRunning() {
    return state_ == Running;
}


std::string Pomodoro::getFormattedRemainingTime() {
    return fmt::format("{:%M}:{:%S}",
                       std::chrono::duration_cast<Minutes>(remainingTime_),
                       std::chrono::duration_cast<Seconds>(remainingTime_));
}
