//
// Created by hg on 1/11/24.
//

#ifndef POMODORO_H
#define POMODORO_H

#include <chrono>
#include <string>


class Pomodoro {
    /*
     * The Pomodoro class is passive. It depends on an external timer that will call the Pomodoro member functions
     * to update the Pomodoro object's state. IT DOES NOT UPDATE ITSELF.
     * In this project, we use a wxTimer object to perform the state updates of a Pomodoro object when a wxEVT_TIMER
     * event is handled.
     */
enum State {
    Start,
    Running,
    Paused,
    Finished
};

public:
    using SteadyClock = std::chrono::steady_clock;
    using Nanoseconds = std::chrono::nanoseconds;
    using Seconds = std::chrono::seconds;
    using Minutes = std::chrono::minutes;
    using Hours = std::chrono::hours;

    Pomodoro() = delete;
    Pomodoro(const Pomodoro& pomodoro) = default;
    Pomodoro(Minutes m, Seconds s);

    void start();
    void stop();
    bool isFinished();
    void updateRemainingTime();
    SteadyClock::duration getRemainingTime();
    std::string getFormattedRemainingTime();
    bool isRunning();

    State state_{Start};

private:
    Minutes minutes_{};
    Seconds seconds_{};
    std::chrono::time_point<SteadyClock> targetTime_{};
    SteadyClock::duration remainingTime_{minutes_ + seconds_};
};

#endif //POMODORO_H
