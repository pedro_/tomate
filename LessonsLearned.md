# Lessons learned

## wxWidgets
Panels (`wxPanel`) and Sizers (`wxSizer`) are fundamental parts of wxWidgets.
Panels are used to *contain* other controls (buttons, sliders, text boxes, and so on). By *contain* I mean being the parent
of other controls. Remember that wxWidgets, just like other other UI toolkits) create the layouts using a parent-children
hierarchy.

When you create a control, you have to specify whose its parent is in the first parameter. For instance, the `wxButton`
constructor is

```c++
wxButton::wxButton(
            wxWindow *          parent,
            wxWindowID          id,
            const wxString &    label = wxEmptyString,
            const wxPoint &     pos = wxDefaultPosition,
            const wxSize &      size = wxDefaultSize,
            long                style = 0,
            const wxValidator & validator = wxDefaultValidator,
            const wxString &    name = wxButtonNameStr 
            )
```

When you specify the parent, you create the parent-child hierarchy. Panels are a `wxWindow`.

Panels contain controls, but they don't know how to arrange them. For this, Panels need a Sizer.
There are different types of Sizers:
`wxSizer`, `wxBoxSizer`, `wxStaticBoxSizer`, `wxStdDialogButtonSizer`, `wxWrapSizer`, `wxGridSizer`, `wxFlexGridSizer`, `wxGridBagSizer`.
All of them allow you to arrange the children of the respective Panel in a different manner.
To add a child, Sizers provide an overloaded `Add` method. One of the signatures is: 
```c++
wxSizerItem* wxSizer::Add(
                wxWindow *  window,
                int  	    proportion = 0,
                int  	    flag = 0,
                int  	    border = 0,
                wxObject *  userData = nullptr 
                )
```

Where:
- `window`: is the child control (button, slider, etc.) you want to add to the sizer. 
- `proportion`: indicates how much space the control will take with respect to its siblings when the main window is resized. 
If you left this as 0, that control being added won't expand if you resize the main window. If all the controls added
have the same value for proportion other than 0, then all of them will occupy the space with the same proportion.
- `flag`: here you can specify the placement of the control, if it expands in all directions, and so on. Some of the flags
used are: wxEXPAND, wxSHAPED, wxALIGN_CENTER. A full explanation is given in [wxBoxSizer](https://docs.wxwidgets.org/latest/classwx_sizer.html).

You can add a Sizer to another Sizer, however, you need to use a different constructor:
```c++
wxSizerItem* wxSizer::Add(
        wxSizer *   sizer,
        int         proportion = 0,
        int         flag = 0,
        int         border = 0,
        wxObject *  userData = nullptr 
        )
```

Once you added the children, you need to set the `wxSizer` to the corresponding `wxPanel` using the function
```c++
void SetSizer(wxSizer *sizer, bool deleteOld=true)
```

**Resources**
- [wxBoxSizer](https://docs.wxwidgets.org/latest/classwx_sizer.html)

### Set a Sizer to a FlexGridSizer item
I ran into layout problems when trying to set a `wxBoxSizer` to a `wxPanel` that was part of a `wxFlexGridSizer`. The problem
occurred if I set it AFTER Panels were added to the Flex Grid Sizer.

This is what I tried: the Panels are added to the Flex Grid Sizer and then, I access the element I'm interested in and
try to append more elements to it. The problem I encountered is that the elements returned by `flexSizer->GetItem(4)` are
of type `SizerItem` and not whatever the original element is. Do I need a cast? Do I have to access something inside that
type and also cast it?

The resulting UI using this method was that the Panel 4 (the center) was not in the right position. However, the elements
that were contained (the `wxStaticText` and the `wxButton`) were indeed at the center.

```c++
    // Creation of the Flex Grid Sizer and setting panels to it
    const auto margin{1};
    auto *flexSizer = new wxFlexGridSizer(3, 3, margin, margin);

    // Columns and rows around the center block will be empty but growable
    flexSizer->AddGrowableCol(0, 1);
    flexSizer->AddGrowableCol(2, 1);
    flexSizer->AddGrowableRow(0, 1);
    flexSizer->AddGrowableRow(2, 1);

    for (int i{0}; i < 3; ++i) {
        for (int j{0}; j < 3; ++j) {
            auto *block = new wxPanel(mainPanel, wxID_ANY, wxDefaultPosition, wxSize{0,0});
            flexSizer->Add(block, 1, wxEXPAND);
        }
    }
    
    // Get the item of of the center and add controls to it
    auto *infoPanel = flexSizer->GetItem(4);
    infoPanel->SetMinSize(startingSize);
    
    timerDisplay = new wxStaticText(infoPanel->GetWindow(), wxID_ANY, app_context_.pomodoro_.getFormattedRemainingTime().c_str(),
        wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);

    startResume = new wxButton(infoPanel->GetWindow(), wxID_ANY, wxT("Start"));

    auto *infoSizer = new wxBoxSizer(wxVERTICAL);
    infoSizer->Add(timerDisplay, 1, wxSHAPED | wxALIGN_CENTER);
    infoSizer->Add(startResume, 1, wxSHAPED | wxALIGN_CENTER);
    infoPanel->AssignSizer(infoSizer);
```

<br><br>

**The solution was to set everything AT THE MOMENT the Flex Grid Sizer was created**. Like this:
```c++
    const auto margin{1};
    auto *flexSizer = new wxFlexGridSizer(3, 3, margin, margin);

    // Columns and rows around the center block will be empty but growable
    flexSizer->AddGrowableCol(0, 1);
    flexSizer->AddGrowableCol(2, 1);
    flexSizer->AddGrowableRow(0, 1);
    flexSizer->AddGrowableRow(2, 1);

    for (int i{0}; i < 3; ++i) {
        for (int j{0}; j < 3; ++j) {
            auto *block = new wxPanel(mainPanel, wxID_ANY, wxDefaultPosition, wxSize{0,0});

            // Place the controls in the center slot
            if ((i == 1) && (j == 1)) {
                timerDisplay = new wxStaticText(block, wxID_ANY, app_context_.pomodoro_.getFormattedRemainingTime().c_str(),
        wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER);

                startResume = new wxButton(block, wxID_ANY, wxT("Start"));

                auto *infoSizer = new wxBoxSizer(wxVERTICAL);
                infoSizer->Add(timerDisplay, 1, wxSHAPED | wxALIGN_CENTER);
                infoSizer->Add(startResume, 1, wxSHAPED | wxALIGN_CENTER);
                block->SetSizer(infoSizer);

                // Set a minimun size for the center block
                block->SetMinSize(startingSize);
            }

            flexSizer->Add(block, 1, wxEXPAND);
        }
    }
```

Is this a bug? What did I do wrong? I suppose it should be possible.


### Text vertical alignment
It's impossible to vertically center the content of a wxStaticText using flags like `wxALIGN_CENTER`. This only works for
horizontal alignment. Instead, you must insert other resizable containers around the control. For instance, if your text
is in a vertical Box Sizer
- insert Stretch Spacers between your elements
- Don't use proportion, leave it to `0`. Of course, this might depend on your needs 
- set the flag `wxALIGN_CENTER` so it's horizontally centered
- DON'T USE the flag `wxSHAPED` if the text you display will vary depending on some actions. This could affect the text displayed

```c++
auto* infoSizer = new wxBoxSizer(wxVERTICAL);
// NOTE: don't use the flag wxSHAPED, it will cut off parts of strings of the phaseDisplay control
infoSizer->AddStretchSpacer();
infoSizer->Add(phaseDisplay, 0, wxALIGN_CENTER);
infoSizer->AddStretchSpacer();
infoSizer->Add(workCountDisplay, 0, wxALIGN_CENTER);
infoSizer->AddStretchSpacer();
infoSizer->Add(timerDisplay, 0, wxALIGN_CENTER);
infoSizer->AddStretchSpacer();
infoSizer->Add(startStopButton, 0, wxALIGN_CENTER);
infoSizer->AddStretchSpacer();
```



## Random numbers and distributions

In C++ you can use functions from the header `<random>` to access different functions to generate pseudo-random numbers.

Example. Obtain random numbers from a uniform distribution from the range 0-255.
```c++
std::random_device seed;
std::mt19937 randGen(seed()); // mersenne_twister_engine seeded with rd()
std::uniform_int_distribution<int> colorDistribution(0,255);
```

**Resources**
- https://cplusplus.com/reference/random/

