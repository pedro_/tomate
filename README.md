# Tomate

A C++ app to do the Pomodoro technique.

## Usage

**NOTE: Currently only tested on Linux.**

Just click on `Start` to begin the timer.

<p>
  <img src="docs/res/ui-start-focus-phase.png" alt="Tomate app. Starting view.">
</p>

Hit `Stop` if you need to restart the current phase (either Focus session, Short Rest or Long Rest).

<p>
  <img src="docs/res/ui-stop-focus-phase.png" alt="Tomate app. Running.">
</p>
![img.png](img.png)

## Requirements

### **[{fmt}](https://github.com/fmtlib/fmt) (v10.2.0)**

Tomate already includes the required files from the fmt library (v10.2.0). They are located in the
directory `third-party/fmt`.

### **wxWidgets**

The library must be installed in your system

```shell
sudo apt install libwxgtk3.0-gtk3-dev
```

## TODO

### UI improvements to consider

- Improve the UI. Still don't know how to improve it.
  - Maybe instead of displaying just the number of pomodoros, we could use the terminology of Solanum: `Lap n`. I'm also considering `Session n`
  - Change the text color of either the `phaseDisplay`, the `timerDisplay` or both, depending on:
    - the timer is running
    - the phase/stage

### UX improvements / features to consider

- Generate statistics.
  - Keep track of how many focus sessions (pomodoros) you have made in a day. Keep a log and plot them
    - Or plot the time worked each day and the time of rest (short + long)
    - how many days we'll keep track of?
- Add a settings menu or some method to change the timer default values if the user wants to.
- Support for pause?
  - AFAIK the wxTimer doesn't have a Pause() method, I would need to implement something new.
  - Also, the pomodoro technique recommends to not pause. If you pause for a long time, cancel the pomodoro and start
    again, just like we do right now. However, in the real world this is sometimes impossible.
- Support for skip?
  - Solanum allows you to jump to the next phase if you want to.
  - This could cause problems with the statistics. It will depend on how we track them.

[//]: # (### Infra improvements)

[//]: # ()
[//]: # (- Find a better way to solve the dependency on the asio library. **NOTE:** right now we don't have a dependency with this)

[//]: # (  library. We are using a wxTimer to do the periodic events &#40;updating the UI each second&#41;)

## References

- [Pomodoro Technique - Wikipedia](https://en.wikipedia.org/wiki/Pomodoro_Technique)
- [{fmt}](https://github.com/fmtlib/fmt)

